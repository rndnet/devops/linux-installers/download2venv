#!/bin/bash

. common_params.sh
if [ ! -f "common_params.sh" ];  then
   echo "File common_params.sh not found!" ;  exit 1 ;
fi

if [ -d "$venv_src" ]; then

    clear
    cd $venv_src
    ls -1
    echo
    read -e -p  "Enter virtual env: " venv_cur
    read -e  -p  "Enter command: " cmd_cur
    source $venv_cur/bin/activate
    if [ $? = 0 ]; then
      echo "Current python: '"$(which python)"'"
      echo
      eval ${cmd_cur}
      deactivate
    fi
    echo
    echo "Current python: '"$(which python)"'"
else
    echo
    echo $venv_src' not exists!'
fi




