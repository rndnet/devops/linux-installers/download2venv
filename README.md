Download rndnet platform from repository and install to virtual python environment.
----------------------------------------------------------------------------------

1. cp libs/common_params_skel.sh common_params.sih

2. Set valid packages and venv_src directoties, cloud_name and python_version in common_params.sh.

3. Set valid snode_python_load and snode_python_unload commands in common.sh. If you do not want use modules just comment it.

4. Set valid params in server.conf

5. Set valid params in save-configs-arch.sh. (Optional)
6. Set valid params in run-module-julia-1.0.3.sh. (Optional)

7. Install
   Run run-download-modules.sh 
   Run run-update-clients.sh
   Run run-update-smaster-no-restart.sh
   Run run-update-snodes.sh*

8. Database initialize (https://docs.google.com/document/d/1Xr59uoG0GmuoH-MOkSLAVlAcq5zyYDJRNwtp0jYdB6Q/  Сервер->Настройка БД и Инициализация облака) 

9. Test server run
   - cp libs/server.conf server.conf
   - cp libs/start-server.sh start-server.sh
   - Set valid params in server.conf (https://docs.google.com/document/d/1Xr59uoG0GmuoH-MOkSLAVlAcq5zyYDJRNwtp0jYdB6Q/  Сервер->Настройка и запуск)
   - Run start-server.sh


10. Client run:
    - Use run-client.sh when run from console
    - Use $venv_client/bin/cloudview when create shortcut (and set working directory to $venv_client/bin)


Scripts/Files description
-----------------
1.  run-rndnet_console.sh - run admin console.

2.  update-all.sh - full update: packets, database schema, all virtual python environments (VPE).

3.  run-download-modules.sh - download last packets

4.  common_params.sh - main config

5.  check-installed.sh - list all python modules in all VPE
6.  check-installed_rndnet.sh - list all rndnet modules in all VPE

  Update VPE (only after run-download-modules.sh)
7.  run-update-clients.sh - install/update clients VPE
8.  run-update-developers.sh - install/update developers VPE
9.  run-update-smaster.sh - install/update server/scheduler VPE and restart services.
10. run-update-smaster-no-restart.sh - install/update server/scheduler VPE.
11. run-update-snodes.sh - install/update compute VPE
12. run-update-database.sh - update database schema (use only after server/scheduler VPE were updated).)

  VPE administration
13. run-command-in-venv.sh - run any console command in choosen VPE
14. run-update-one-venv.sh - create/update choosen VPE

  Application administration   
15. run-client.sh - run client application
16. start-server.sh - server manual run
17. start-rndnet-services.sh - start rndnet services if exist
18. stop-rndnet-services.sh - stop rndnet services if exist
19. add_user.sh - add new user


20. save-configs-arch.sh - make/update archive
21. run-module-julia-1.0.3.sh - run julia  (not installed by default)

