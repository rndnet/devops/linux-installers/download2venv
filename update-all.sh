#!/bin/bash
clear
curdir=$(pwd)

#./check-status.sh

read -t 120 -n 1 -s -r -p "Press any key to continue for update download"
echo
cd $curdir
./run-download-modules.sh
echo "Update downloaded"
echo

read -t 120 -n 1 -s -r -p "Press any key to continue for client update"
echo
cd $curdir
./run-update-clients.sh
echo "Clients updated"
echo

read -t 120 -n 1 -s -r -p "Press any key to continue for snode update"
cd $curdir
./run-update-snodes.sh
echo "Snode updated"
echo

read -t 120 -n 1 -s -r -p "Press any key to continue for developers update"
echo
cd $curdir
./run-update-developers.sh
echo "Developers updated"
echo

read -t 120 -n 1 -s -r -p "Press any key to continue for server update"
echo
cd $curdir
./run-update-smaster.sh
echo "Server updated"
echo

read -t 120 -n 1 -s -r -p "Press any key to continue for database update"
echo
cd $curdir
./run-update-database-and-log.sh
echo "Database updated"

