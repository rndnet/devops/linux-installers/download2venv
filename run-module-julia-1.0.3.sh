#!/bin/bash

#clear
#echo 'Remote julia command examples:'
#echo 'Add BFJ'
#echo '    import Pkg;Pkg.add(Pkg.PackageSpec(url="https://gitlab.com/rndnet/apps/BFJ.jl",rev="master"))'
#echo 'Remove BFJ'
#echo '    import Pkg; Pkg.rm("BFJ")'
#echo 'Add HDF5'
#echo '    import Pkg;Pkg.add("HDF5")'
#echo 'Installed list'
#echo '    import Pkg;Pkg.installed()'
#echo

#read -e  -p  "Enter command: " cmd_cur

#JULIA_COMMAND="julia -E '${cmd_cur}'"

##ssh -tt gtech@epyc-login0  srun --pty bash << EOF
##ssh -tt gtech@epyc-login0  srun bash << EOF # show all commands for debug
#ssh -T gtech@epyc-login0  srun bash << EOF
#  module load  julia/julia-1.0.3-x86_64
#  export JULIA_DEPOT_PATH=/ceph/home/r003/rndnet/julia-home/julia-1.0.3
  
#  ${JULIA_COMMAND}
  
#  module unload  julia/julia-1.0.3-x86_64
#  exit
#EOF

### Local variant
module load  julia/julia-1.0.3-x86_64
export JULIA_DEPOT_PATH=/nfs0/r003/rndnet/julia-home/julia-1.0.3
#julia -E 'import Pkg;Pkg.add(Pkg.PackageSpec(url="https://gitlab.com/rndnet/apps/BFJ.jl",rev="master"))'
julia
module unload  julia/julia-1.0.3-x86_64

