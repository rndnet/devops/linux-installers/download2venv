#!/bin/bash
cd libs
. common.sh
if [ ! -f "common.sh" ];  then
         echo "File common.sh not found!" ;  exit 1 ;
fi

echo -e "\n${RED}Starting server and  schedulers...${NC}\n"
start_array smaster_services[@]

