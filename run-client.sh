#!/bin/bash

. common_params.sh
if [ ! -f "common_params.sh" ];  then
   echo "File common_params.sh not found!" ;  exit 1 ;
fi

if [ -f "$venv_client/bin/activate" ];  then
    echo "Client virtual env"
    source $venv_client/bin/activate
    if [ $? = 0 ]; then
      cloudview 
      deactivate
    fi
    echo "Current python: '"$(which python)"'"
else
    echo
    echo $venv_client'/bin/activate not exists!'
fi





