. common.sh 
if [ ! -f "common.sh" ];  then
    echo "File"$(pwd)" /common.sh not found!" ;  exit 1 ;
fi


if [ ! -d "$venv_sr" ];  then
    echo $venv_src' not exists!'
    exit 1
fi


clear
echo -e "${RED}Current virtual env: ${NC}"
ls -1 $venv_src/

echo 
read -e -p "Enter new venv name: " venv_personal
venv_personal=${venv_src}/${venv_personal}

echo "Choose RnDnet bundle:"
echo "------------------------------------------"
echo "1) Compute: "   ${compute_node_bundle[@]}
echo "2) Client: "    ${client_bundle[@]}
echo "3) Server: "    ${smaster_bundle[@]}
echo "4) Developer: " ${develop_bundle[@]}
echo "5) Full: "      ${super_bundle[@]}
echo "------------------------------------------"
echo "0) Exit"

read doing 

case $doing in
    1)
       personal_bundle=( "${compute_node_bundle[@]}")
       ;;
    2)
       personal_bundle=( "${client_bundle[@]}")
       ;;
    3)
       personal_bundle=( "${smaster_bundle[@]}")
       ;;
    4)
       personal_bundle=( "${develop_bundle[@]}")
       ;;
    5)
       personal_bundle=( "${super_bundle[@]}")
       ;;
    0)
       exit
       ;;
    *)
      echo Unknown bundle
      exit  
      ;;
esac 

###-------------------------------------------
#cur_python=$(which python)
echo "Choose python:"
echo "------------------------------------------"
echo "1) Current: "   $(which python)
echo "2) anaconda3/python-3.6.9"
echo "------------------------------------------"
echo "0) Exit"

read doing


case $doing in
    1)
       unload_command=""
       ;;
    2)
       
       echo "module load anaconda3/python-3.6.9"
       module load anaconda3/python-3.6.9
       which python
       unload_command="module unload anaconda3/python-3.6.9"
       ;;
    0)
       exit
       ;;
    *)
      echo Unknown python
      exit
      ;;
esac

###-------------------------------------------

echo Install modules: ${personal_bundle[@]}
install_array personal_bundle[@]  ${venv_personal}

echo ${unload_command}
eval ${unload_command}

echo "Current python" $(which python)
