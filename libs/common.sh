. ../common_params.sh
if [ ! -f "../common_params.sh" ];  then
     echo "File ../common_params.sh not found!" ;  exit 1 ;
fi

CUR_USER=$USER
RED='\033[0;31m'
NC='\033[0m' # No Color

echo ---------------------------------------------
echo $(date)
echo 

if [ $CUR_USER = 'root' ]
then
    echo Do not use root user here!
    exit
fi

echo Sources dir: $src
echo Smaster services: ${smaster_services[*]}
echo Client bundle: ${client_bundle[*]}
echo Compute node bundle: ${compute_node_bundle[*]}
echo Developer bundle: ${develop_bundle[*]}
echo Smaster bundle: ${smaster_bundle[*]}
echo Super bundle: ${super_bundle[*]}
#echo Update server: ${update_server}
echo Cloud name: ${cloud_name}
echo
echo Snode venv: ${venv_snode[*]}
echo Client venv: ${venv_client[*]}
echo Developer venv: ${venv_developer[*]}
echo Smaster venv: ${venv_smaster}

echo Snode python load command:   ${snode_python_load}
echo Snode python unload command: ${snode_python_unload}

echo --------------------------------------------

CUR_DIR=$(pwd)

function install_array() {

    local  modules=("${!1}")
    #echo "${modules[@]}"

    local venv_dir="${2}"
    
    if [ ! -d ${venv_dir} ]; then
        echo -e "\nVirtual python env not exist yet: '"${venv_dir}"'"
        mkdir -p -v ${venv_dir}
        
	python3 -m venv ${venv_dir}

        if [ $? != 0 ]; then
           echo -e "\nUse alternative virtual python env creation\n"
           python3 -m venv ${venv_dir} --without-pip --clear
	   source ${venv_dir}/bin/activate
	   python get-pip.py
	   deactivate
        fi

        echo -e "\n${RED} Virtual python env created in '${venv_dir}'${NC}"
    fi

    source $venv_dir/bin/activate
    echo -e "\n${RED} Virtual python env activated in '${venv_dir}'${NC}"
    echo -e "\nVirtual python path: "$(which python)

    for i in "${modules[@]}"
    do
        echo -e "\n${RED} Install rndnet/$i in '${venv_dir}'  ${NC}"
	pip3 install -r ${packages}/${i}.requirements.txt
        pip3 install -U --upgrade-strategy eager  ${i} --no-index --find-links ${packages}
    done

    deactivate	
    echo -e "\nVenv ${venv_dir} deactivated. Python path: "$(which python)

}

function stop_array() {

    local  modules=("${!1}")
    #echo "${modules[@]}"

    for i in "${modules[@]}"
    do
        echo -e "${RED} Stop $i service ${NC}"
        sudo systemctl stop $i
    done
}

function start_array() {
    local  modules=("${!1}")
    #echo "${modules[@]}"

    for i in "${modules[@]}"
    do
        echo -e "${RED} Start $i service ${NC}"
        sudo systemctl start $i
        sleep 20
        systemctl status $i
    done
}

function client_dialog() {
        local text="$1"
        read -p "$1: " UPDATE_CLIENT

        if [ -n "$UPDATE_CLIENT" ]
        then
            echo -e  "${RED}Install current version of rndnet to ${NC}"$UPDATE_CLIENT
        else 
            echo Node name is empty!
            exit
        fi
}
