python_version=python3.6                # Python version in $venv_smaster 
packages=~/rndnet/packages  # Download sources to this directory
venv_src=~/rndnet/venv      # Create virtual python environments in this directory
cloud_name=cloud_rndnet                 # Cloud name - how it registered in rndnet_console

smaster_services=( )  # Services names
#smaster_services=( rndnet_server_local rndnet_scheduler_local )  # Services names

#snode_python_load="module load anaconda3/python-3.6.9"       #  python module load command
#snode_python_unload="module unload anaconda3/python-3.6.9"   #  pythob module unload comman

venv_snode=( $venv_src/snode-modules )
venv_client=( $venv_src/client-modules )
venv_developer=( $venv_src/developer-modules )
venv_smaster=$venv_src/smaster-modules

SCHEME_FILE=$venv_smaster/lib/${python_version}/site-packages/rndnet_server/console/schema.py

client_bundle=( rndnet-client )
develop_bundle=( rndnet-server rndnet-client )
compute_node_bundle=( bfj )
smaster_bundle=( rndnet-server rndnet-scheduler )
super_bundle=( rndnet-server rndnet-client )

rndnet_rep=https://server1.rndnet.net/static/simple

