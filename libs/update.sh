. common.sh
if [ ! -f "common.sh" ];  then
    echo "File"$(pwd)" /common.sh not found!" ;  exit 1 ;
fi 

if [ -f "$SCHEME_FILE" ];  then
     date_old=$(stat -c %y $SCHEME_FILE)
else
    echo $SCHEME_FILE' not exist! Can not check database scheme changes!'
fi

case $1  in
     download)
          mkdir -p $packages
          wget -O $packages/bfj.requirements.txt                $rndnet_rep/bfj/requirements.txt
          wget -O $packages/rndnet-scheduler.requirements.txt   $rndnet_rep/rndnet-scheduler/requirements.txt
          wget -O $packages/rndnet-client.requirements.txt      $rndnet_rep/rndnet-client/requirements.txt
          wget -O $packages/rndnet-server.requirements.txt      $rndnet_rep/rndnet-server/requirements.txt

          pip3 download -d $packages/ --index-url $rndnet_rep bfj
          pip3 download -d $packages/ --index-url $rndnet_rep rndnet-scheduler
          pip3 download -d $packages/ --index-url $rndnet_rep rndnet-client
          pip3 download -d $packages/ --index-url $rndnet_rep rndnet-server
          ;;

     clients)
          for cur_client in "${venv_client[@]}"
          do
             echo -e "\n${RED} Install client  bundle to ${cur_client} ${NC}"
             install_array client_bundle[@] ${cur_client}
          done
          ;;

     snodes)

          for cur_snode in "${venv_snode[@]}"
          do
             echo -e "\n${RED} Install snode bundle to ${cur_snode} ${NC}"
             install_array compute_node_bundle[@] ${cur_snode}
          done
          ;;

     developers)
          for cur_dev in "${venv_developer[@]}"
          do
             echo -e "\n${RED} Install developer bundle to ${cur_dev} ${NC}"
             install_array develop_bundle[@] ${cur_dev}
          done
          ;;

     smaster)
                    
	  echo -e "\nSmaster node"

          install_array smaster_bundle[@] $venv_smaster

          echo IS_RESTART_RNDNET_SERVICES=${IS_RESTART_RNDNET_SERVICES}
          if [ ${IS_RESTART_RNDNET_SERVICES} = "true" ]
          then
            echo -e "\n${RED}Stopping server and schedulers...${NC}\n"
            stop_array smaster_services[@]
            echo -e "\n${RED}Starting server and schedulers...${NC}\n"
            start_array smaster_services[@]
          fi


          ;;
     *)
          echo -e  "${RED}Unknown command: $1 ${NC}"
          echo Use commands: clients, developers, snodes, smaster, download
          exit
          ;;
esac


if [ -f "$SCHEME_FILE" ];  then
    date_new=$(stat -c %y $SCHEME_FILE)
    if [ "$date_old" != "$date_new" ] ; then
        echo -e "\n ${SCHEME_FILE} ${RED}has changed! You must run database update script!${NC}"
    fi
else
    echo $SCHEME_FILE' not exist! Can not check database scheme changes!'
fi



