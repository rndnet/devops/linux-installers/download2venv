#!/bin/bash
arch=$(pwd)/../configs

RNDNET_SERVICE_POSTFIX=local
MINIO_SERVICE=minio

mkdir -p $arch/etc/default
cp -vu /etc/default/minio.cfg $arch/etc/default/${MINIO_SERVICE}.cfg

mkdir -p $arch/etc/nginx
cp -vu /etc/nginx/nginx.conf $arch/etc/nginx/nginx.conf

mkdir -p $arch/etc/nginx/sites-enabled
cp -vu /etc/nginx/sites-enabled/* $arch/etc/nginx/sites-enabled/

mkdir -p $arch/etc/rndnet_scheduler
cp -vu /etc/rndnet_scheduler/* $arch/etc/rndnet_scheduler/

mkdir -p $arch/etc/rndnet_server
cp -vu /etc/rndnet_server/* $arch/etc/rndnet_server/

mkdir -p $arch/etc/systemd/system
cp -vu /etc/systemd/system/minio.service $arch/etc/systemd/system/${MINIO_SERVICE}.service
cp -vu /etc/systemd/system/rndnet_scheduler_local.service $arch/etc/systemd/system/rndnet_scheduler_${RNDNET_SERVICE_POSTFIX}.service
cp -vu /etc/systemd/system/rndnet_server_local.service $arch/etc/systemd/system/rndnet_server_${RNDNET_SERVICE_POSTFIX}.service

cp -vu common_params.sh $arch/common_params.sh
cp -vu save-configs-arch.sh $arch/save-configs-arch.sh

mkdir -p $arch/$HOME/.config/blockflow
cp -vu $HOME/.config/blockflow/rndnet_console.json $arch/$HOME/.config/blockflow/rndnet_console.json
