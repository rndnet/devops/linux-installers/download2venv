#!/bin/bash

. common_params.sh
if [ ! -f "common_params.sh" ];  then
   echo "File common_params.sh not found!" ;  exit 1 ;
fi



if [ -d "$venv_src" ];  then

    for f in $(ls -d $venv_src/*/);
    do

     if [ -f $f/bin/activate ]; then
       echo $f" virtual env"
       source $f/bin/activate
       if [ $? = 0 ]; then 
         pip3 list | grep -e rndnet- -e bfj
         deactivate
       fi
     fi

    done
    echo "Current python: '"$(which python)"'"
else
    echo
    echo $venv_src' not exists!'
fi


