#!/bin/bash
. common_params.sh
if [ ! -f "common_params.sh" ];  then
   echo "File common_params.sh not found!" ;  exit 1 ;
fi

if [ -z "$snode_python_load" ]
then
    cd libs
    bash update.sh snodes 2>&1 | tee -a ../logs/logging-snodes-update.log
else
    echo "Current python:" $(which python)
    echo $snode_python_load

    eval ${snode_python_load}
    which python

    cd libs
    bash update.sh snodes 2>&1 | tee -a ../logs/logging-snodes-update.log

    echo $snode_python_unload
    echo "Current python:" $(which python)
fi

