#!/bin/bash

. common_params.sh
if [ ! -f "common_params.sh" ];  then
   echo "File common_params.sh not found!" ;  exit 1 ;
fi

if [ -f "$venv_smaster/bin/activate" ];  then
    echo "Smaster virtual env"
    source $venv_smaster/bin/activate
    if [ $? = 0 ]; then
      read -p "Enter new user name for '"${cloud_name}" 'cloud: " user_name
      rndnet_console user add $user_name --cloud=${cloud_name}
      deactivate
    fi

    echo "Current python: '"$(which python)"'"
else
     echo
     echo $venv_smaster'/bin/activate not exists!'
fi





